from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
    # TabularInline deixa formatado em tabela
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    # lista os dados: pergunta, data da publicação, se foi publicado recente
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    # Cria um filtro lateral que depende do tipo do campo pelo qual você está filtrando.
    # Como pub_date é uma instância da classe DateTimeField,
    # então o Django coloca os filtros : “Qualquer data”, “Hoje”, “Últimos 7 dias”, “Esse mês”, “Esse ano”.
    list_filter = ['pub_date']
    # CAMPO DE BUSCA, COM OS PARAMETROS ABAIXO: No caso, coloquei por pergunta e data
    search_fields = ['question_text', 'pub_date']


admin.site.register(Question, QuestionAdmin)
